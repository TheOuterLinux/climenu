#!/bin/bash
#
# Title: image-edit
# Author: TheOuterLinux
# Purpose: Edit images from the command-line using the power of ImageMagick
#

#####[Variables - Environment]#####
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
TTYCHECK=$(tty)
TTYCHECK=${TTYCHECK%/*}
TTYCHECK=${TTYCHECK##*/} #If NOT in TTY, TTYCHECK output should be "pts"
SYSTEMTYPE=$(uname -m) #Check system architecture   

#####[Create files or folders]#####
if [ -d "/tmp/ImageMagick" ]; then mkdir "/tmp/ImageMagick"; fi
if [ ! -d "/tmp/ImageMagick" ]; then mkdir "/tmp/ImageMagick"; fi

#####[Variables - Colors]#####
CLEAN="\e[0m"
GREEN="\e[92m"
BLUE="\e[94m"
YELLOW="\e[93m"
RED="\e[91m"

#####[Variables - Default Programs]#####
FILEMANAGER=""
EDITOR=""
VIEWER=""

#This section is for if no default pograms are set above.
#The order of preference is reversed.
if [ "$FILEMANAGER" = "" ]
then
    if hash mc 2>/dev/null; then FILEMANAGER="mc"; fi
    if hash ranger 2>/dev/null; then FILEMANAGER="ranger"; fi
fi

if [ "$EDITOR" = "" ]
then
    if hash pico 2>/dev/null; then EDITOR="pico"; fi
    if hash nano 2>/dev/null; then EDITOR="nano"; fi
fi

if [ "$TTYCHECK" != "pts" ] && [ "$DISPLAY" = "" ]
then
    if [ "$VIEWER" = "" ]
    then
        if hash fbi 2>/dev/null;then VIEWER="fbi"; fi
    fi
else
    if [ "$VIEWER" = "" ]
    then
        VIEWER="feh"
    fi
fi

menu_first() {
    clear
    printf ${GREEN}
    figlet " $1"
    printf ${CLEAN}
    printf ${BLUE}
    echo
    echo " $2"
    printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' "-"
    printf ${CLEAN}
    printf ${YELLOW}
}
menu_second() {
    printf ${RED}
    echo
    if [ "$TTYCHECK" != "pts" ]
    then
        echo " q. Quit    h. Help    l. Lock    e. Editor    f. File Manager    m. Main"
    else
        echo " q. Quit    h. Help    e. Editor    f. File Manager    m. Main"
    fi
    printf ${CLEAN}
    printf ${BLUE}
    printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' "-"
    printf ${CLEAN}
    #echo
    echo -n " Please choose a menu item: "
    printf ${GREEN}
    read "$1"
    printf ${CLEAN}
}
file_chooser() {
   # FILE="$(dialog --stdout --title "Please choose a file" --fselect $HOME/ 0 "${COLUMNS:-$(tput cols)}")"
    FILE="$(find "$1" -not -path '*/\.*' -type f | fzy)"
}
file_chooser_two() {
    echo "$2"
    echo
    FILE="$(find "$1" -not -path '*/\.*' -type f | fzy)"
    echo
    echo "$3"
    FILE_SECOND="$(find "$1" -not -path '*/\.*' -type f | fzy)"
}
filecheck (){
	while :
	do
	    menu_first "ImageMagick" "Please choose an image file to $1:"
        if file_chooser "$HOME"
        then
            if [ "$TTYCHECK" != "pts" ] && [ "$DISPLAY" = "" ]
            then 
                "$VIEWER" "$FILE"
            else
                "$VIEWER" "$FILE" &
            fi
            menu_first "ImageMagick" "Was that the correct image to edit?"
            echo " 1) Yes"
            echo " 2) No"
            echo " 3) Never mind"
            menu_second "menu_opt"
            case $menu_opt in
                1|yY*)
                    cp "$FILE" /tmp/ImageMagick/
                    break
                ;;
                2|nN*)
                    if kill $(pgrep -l -n "$VIEWER" | awk '{print $1}') 2>/dev/null; then kill $(pgrep -l -n "$VIEWER" | awk '{print $1}'); fi
                ;;
                *|3|b*|m*)
                    if kill $(pgrep -l -n "$VIEWER" | awk '{print $1}') 2>/dev/null; then kill $(pgrep -l -n "$VIEWER" | awk '{print $1}'); fi
                    main
                ;;
            esac
        else
            main
        fi
	done
}
fileedit() {
    if [ "$TTYCHECK" != "pts" ] && [ "$DISPLAY" = "" ]
    then 
        "$VIEWER" "/tmp/ImageMagick/${FILE##*/}"
    else
        "$VIEWER" "/tmp/ImageMagick/${FILE##*/}" &
    fi
    menu_first "ImageMagick" "Did that look ok?"
    echo " 1) Yes"
    echo " 2) No"
    menu_second "menu_opt"
    case $menu_opt in
        1|y*|Y*)
                mv "/tmp/ImageMagick/${FILE##*/}" "${FILE%/*}/${FILE##*/}"
                imagecheck="0"
            ;;
        2|n*|N*)
                if kill $(pgrep -l -n "$VIEWER" | awk '{print $1}') 2>/dev/null; then kill $(pgrep -l -n "$VIEWER" | awk '{print $1}'); fi
                imagecheck="1"
            ;;
        *|b*|m*)
                if kill $(pgrep -l -n "$VIEWER" | awk '{print $1}') 2>/dev/null; then kill $(pgrep -l -n "$VIEWER" | awk '{print $1}'); fi
                main
            ;;
    esac
}
global_help() {
    clear
    printf ${GREEN}
    figlet " Help"
    printf ${BLUE}
    echo
    echo " Main Menu --> Help"
    printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' "-"
    printf ${YELLOW}
    echo " Use the following anywhere when prompted to select a menu item: "
    echo
    echo " m   - Go to main menu"
    echo " b,0 - Go back"
    if [ "$TTYCHECK" != "pts" ]
    then
        echo " l   - Lock all command-lines (tty) with vlock"
    fi
    echo " f   - File Manager ($FILEMANAGER)"
    echo " e   - Text Editor ($EDITOR)"
    echo " q   - Quit"
    printf ${RED}
    echo
    echo " If you get an error regarding framebuffer image viewing, you"
    echo " may need to add your user to the video group. You can do this"
    echo " by running: sudo usermod -a -G video $USER."
    printf ${BLUE}
    printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' "-"
    printf ${YELLOW}
    echo
    echo " When trying to quit certain programs/shells try the following in order:"
    echo "    1. q key"
    echo "    2. CTRL+d"
    echo "    3. Tpye exit or exit() and press ENTER"
    echo "    4. CTRL+c"
    echo "    5. Run htop, F4 to filter out program, F9, 9, ENTER"
    echo "    6. Run 'pkill -f [program]'"
    echo "    7. In the case of VIM/EMACS, break the EMERGENCY glass, grab the fire axe, and good luck..."
    printf ${CLEAN}
    echo
    echo " Press ENTER to go back to menu."
    read pause
}
main() {
while :
do
    menu_first "ImageMagick" "Main Menu"
    if [ "$TTYCHECK" != "pts" ] && [ "$DISPLAY" = "" ]
    then
        echo "  0. Console"
        echo "  1. View Image"
        echo "  2. Blur"
        echo "  3. Convert"
        echo "  4. Compress (jpeg)"
        echo "  5. Contrast"
        echo "  6. Despeckle"
        echo "  7. Enhance - Get rid of noise"
        echo "  8. Flip image (vertical flip ^ v)"
        echo "  9. Flop image (horizontal flop < >)"
        echo " 10. Gamma"
        echo " 11. Image info"
        echo " 12. Monochromatic - Black and White image"
        echo " 13. Posterize - Reduce number of colors"
        echo " 14. Resize"
    else
        echo "  0. Console                 11. Image info"
        echo "  1. View Image              12. Monochromatic - Black and White image"
        echo "  2. Blur                    13. Posterize - Reduce number of colors"
        echo "  3. Convert                 14. Resize"
        echo "  4. Compress (jpeg)"
        echo "  5. Contrast"
        echo "  6. Despeckle"
        echo "  7. Enhance - Get rid of noise"
        echo "  8. Flip image (vertical flip ^ v)"
        echo "  9. Flop image (horizontal flop < >)"
        echo " 10. Gamma"
    fi
    menu_second "menu_opt"
    case $menu_opt in
        0)
            clear
            bash
        ;;
        1) view
        ;;
        2) blr
        ;;
        3) conv
        ;;
        4) comp
        ;;
        5) cont
        ;;
        6) despeck
        ;;
        7) enhan
        ;;
        8) flp
        ;;
        9) flopp
        ;;
       10) gam
        ;;
       11) info
        ;;
       12) mono
        ;;
       13) post
        ;;
       14) res
        ;;
        q|Q|QUIT|Quit|quit|exit|EXIT|Exit)
           clear
           exit 0
        ;;
        l) if [ "$TTYCHECK" != "pts" ];then vlock -a; fi
        ;;
        f) $FILEMANAGER
        ;;
        e) $EDITOR
        ;;
        s|S|SEARCH|search|Search) 
           clear
           printf ${YELLOW}
           figlet " Search"
           printf ${CLEAN}
           echo
           echo -n " Enter search: "
           read search
           echo
           echo " Search results:"
           echo
           apropos "$search"
           echo
           echo " Press ENTER to go back to menu."
           read pause
        ;;
        ?|h|H|HELP|Help|help|man|MAN|Man)
           global_help
        ;;
    esac
done
}
view (){
	while :
	do
	    menu_first "ImageMagick" "Please choose an image file to view:"
        if file_chooser "$HOME"
        then
            "$VIEWER" "$FILE"
            main
        else
            main
        fi
	done
}
blr(){
	imagecheck="1"
	filecheck "blur"
	while [ $imagecheck = "1" ]
	do
	    printf ${YELLOW}
	    echo
	    echo " Radius refers to how how far to push each pixel."
	    echo " Anything higher than 5 is probably too much"
	    printf ${CLEAN}
	    echo
	    echo -n " Radius: "
	    printf ${GREEN}
	    read radius
	    printf ${YELLOW}
	    echo
	    echo " Strength refers to how strong the pixels blur from the start of "
	    echo " each pixel to the end of the chosen radius before fading out."
	    echo " Anything higher than 8 is probably too much."
	    printf ${CLEAN}
	    echo
	    echo -n " Strength: "
	    printf ${GREEN}
	    read strength
	    echo
	    printf ${BLUE}
	    echo " Working the image 'magick.' Please wait..."
	    printf ${CLEAN}
	    mogrify -blur "$radius"x"$strength" "/tmp/ImageMagick/${FILE##*/}"
	    fileedit
	done
}
conv() {
	imagecheck="1"
	filecheck "convert"
	menu_first "ImageMagick" "Choose a file format to convert to:"
	echo " 1) JPEG"
	echo " 2) PNG"
	echo " 3) GIF"
	echo " 4) TIFF"
	echo " 5) PDF"
	menu_second "conv_opt"
	case $conv_opt in
	    1) convert "$FILE" "${FILE%%.*}".jpg ;;
	    2) convert "$FILE" "${FILE%%.*}".png ;;
	    3) convert "$FILE" "${FILE%%.*}".bmp ;;
	    4) convert "$FILE" "${FILE%%.*}".gif ;;
	    5) convert "$FILE" "${FILE%%.*}".tiff ;;
	    6) convert "$FILE" "${FILE%%.*}".pdf ;;
	esac
}
comp(){
	imagecheck="1"
	filecheck "compress"
	while [ $imagecheck = "1" ]
	do
	    printf ${YELLOW}
	    echo
	    echo " This will convert your image to a JPEG."
        echo " The default value is set to 80 (out of 100)."
        printf ${CLEAN}
	    echo
	    echo -n " Compression amount: "
	    printf ${GREEN}
	    read compression
	    re='^[0-9]+$'
	    if [ "$compression" = "" ] || [[ $compression =~ $re ]]
	    then
	        compression="80"
	    fi
	    printf ${YELLOW}
	    echo
	    printf ${BLUE}
	    echo " Working the image 'magick.' Please wait..."
	    printf ${CLEAN}
	    convert "$FILE" -quality $compression "${FILE%%.*}".jpg
	    fileedit
	done
}
cont() {
	imagecheck=1
	filecheck "contrast"
	while [ $imagecheck = "1" ]
	do
	    printf ${YELLOW}
	    echo
	    echo " Use positive numbers to increase the contrast and negative"
	    echo " numbers to decrease the contrast. This uses a 'for loop'"
	    echo " because of the way mogrify handles contrast, so don't be"
	    echo " surprised if this takes a few minutes. However, +10 or -10"
	    echo " values are really high for most cases. Start with maybe"
	    echo " +3 or -3 and see if that's ok first."
	    printf ${CLEAN}
	    echo
	    cp "$FILE" "/tmp/ImageMagick/"
	    echo -n " Contrast amount: "
	    printf ${GREEN}
	    if read amount
		then
		    printf ${CLEAN}
		    absolutevalueamount=$(echo "$amount" | sed 's/+//g') #absolute value convert (remove positive symbol)
		    absolutevalueamount=$(echo "$amount" | sed 's/-//g') #absolute value convert (remove negative symbol)
		    percent=$(( 100 / $absolutevalueamount ))
		    i=1
			if [ $amount -ge 1 ]
			then
				while [ $i -le $absolutevalueamount ]
				do
					#Increase contrast
					if [ "$TTYCHECK" != "pts" ] && [ "$DISPLAY" = "" ]
					then
					    for i in $(seq 0 $percent 100) ; do mogrify -contrast "/tmp/ImageMagick/${FILE##*/}"; sleep 1; echo " $i% complete..."; done
					else
					    for i in $(seq 0 $percent 100) ; do mogrify -contrast "/tmp/ImageMagick/${FILE##*/}"; sleep 1; echo $i | dialog --title "Increasing contrast..." --gauge "Please wait" 6 60 0; done
					fi
					i=$(( $i+1 ))
				done
			fi
			if [ $amount -le -1 ]
			then
				while [ $i -le $absolutevalueamount ]
				do
					#Decrease contrast
					if [ "$TTYCHECK" != "pts" ] && [ "$DISPLAY" = "" ]
					then
					    for i in $(seq 0 $percent 100) ; do mogrify +contrast "/tmp/ImageMagick/${FILE##*/}"; sleep 1; echo "$i% complete..."; done
					else
					    for i in $(seq 0 $percent 100) ; do mogrify +contrast "/tmp/ImageMagick/${FILE##*/}"; sleep 1; echo $i | dialog --title "Decreasing contrast..." --gauge "Please wait" 6 60 0; done
					fi
					i=$(( $i+1 ))
				done
			fi
		else
		    printf ${CLEAN}
			main
		fi
		printf ${CLEAN}
		fileedit
	done
}
despeck() {
	imagecheck=1
	filecheck "despeckle"
	while [ $imagecheck = "1" ]
	do
	    printf ${YELLOW}
	    echo
        echo " Ever noticed a bunch of little speckles in an image? Use"
        echo " this option to get rid of them, or at least as best as it"
        echo " can. A value of 10 is a really high amount."
	    printf ${CLEAN}
	    echo
	    cp "$FILE" "/tmp/ImageMagick/"
	    echo -n " Despeckle amount: "
	    printf ${GREEN}
	    if read amount
		then
		    printf ${CLEAN}
		    absolutevalueamount=$(echo "$amount" | sed 's/+//g') #absolute value convert (remove positive symbol)
		    absolutevalueamount=$(echo "$amount" | sed 's/-//g') #absolute value convert (remove negative symbol)
		    percent=$(( 100 / $absolutevalueamount ))
		    i=1
			if [ $amount -ge 1 ]
			then
				while [ $i -le $absolutevalueamount ]
				do
					if [ "$TTYCHECK" != "pts" ] && [ "$DISPLAY" = "" ]
					then
					    for i in $(seq 0 $percent 100) ; do mogrify -despeckle "/tmp/ImageMagick/${FILE##*/}"; sleep 1; echo " $i% complete..."; done
					else
					    for i in $(seq 0 $percent 100) ; do mogrify -despeckle "/tmp/ImageMagick/${FILE##*/}"; sleep 1; echo $i | dialog --title "Despeckling ${FILE##*/}..." --gauge "Please wait" 6 60 0; done
					fi
					i=$(( $i+1 ))
				done
			fi
		else
		    printf ${CLEAN}
			main
		fi
		printf ${CLEAN}
		fileedit
	done
}
enhan() {
	imagecheck=1
	filecheck "enhance"
	while [ $imagecheck = "1" ]
	do
	    printf ${YELLOW}
	    echo
        echo " By 'enhancing' the image you are essentially getting rid"
        echo " of noise. A value of 10 is a really high amount."
	    printf ${CLEAN}
	    echo
	    cp "$FILE" "/tmp/ImageMagick/"
	    echo -n " Enhancement amount: "
	    printf ${GREEN}
	    if read amount
		then
		    printf ${CLEAN}
		    absolutevalueamount=$(echo "$amount" | sed 's/+//g') #absolute value convert (remove positive symbol)
		    absolutevalueamount=$(echo "$amount" | sed 's/-//g') #absolute value convert (remove negative symbol)
		    percent=$(( 100 / $absolutevalueamount ))
		    i=1
			if [ $amount -ge 1 ]
			then
				while [ $i -le $absolutevalueamount ]
				do
					if [ "$TTYCHECK" != "pts" ] && [ "$DISPLAY" = "" ]
					then
					    for i in $(seq 0 $percent 100) ; do mogrify -enhance "/tmp/ImageMagick/${FILE##*/}"; sleep 1; echo " $i% complete..."; done
					else
					    for i in $(seq 0 $percent 100) ; do mogrify -enhance "/tmp/ImageMagick/${FILE##*/}"; sleep 1; echo $i | dialog --title "Enhancing ${FILE##*/}..." --gauge "Please wait" 6 60 0; done
					fi
					i=$(( $i+1 ))
				done
			fi
		else
		    printf ${CLEAN}
			main
		fi
		printf ${CLEAN}
		fileedit
	done
}
flp() {
	imagecheck=1
	filecheck "flip"
	while [ $imagecheck = "1" ]
	do
	    cp "$FILE" "/tmp/ImageMagick/"
	    mogrify -flip "/tmp/ImageMagick/${FILE##*/}"
	    fileedit
	done
}
flopp() {
	imagecheck=1
	filecheck "flop"
	while [ $imagecheck = "1" ]
	do
	    cp "$FILE" "/tmp/ImageMagick/"
	    mogrify -flop "/tmp/ImageMagick/${FILE##*/}"
	    fileedit
	done
}
gam() {
	imagecheck=1
	filecheck "gamma"
	while [ $imagecheck = "1" ]
	do
	    printf ${YELLOW}
	    echo
        echo " Adjust the gamma of an image. This is a lot like adjusting"
        echo " the brightness or exposure with a hint of contrast, or another "
        echo " way to look at this is 'luminescence.' A normal value for a"
        echo " modern monitor and system is about 2.2."
	    printf ${CLEAN}
	    echo
	    cp "$FILE" "/tmp/ImageMagick/"
	    echo -n " Gamma amount: "
	    printf ${GREEN}
	    if read amount
		then
		    printf ${CLEAN}
			mogrify -gamma  $amount "/tmp/ImageMagick/${FILE##*/}"
		else
		    printf ${CLEAN}
			main
		fi
		printf ${CLEAN}
		fileedit
	done
}
info() {
	filecheck "info"
	cp "$FILE" "/tmp/ImageMagick/"
	mogrify -identify -verbose "/tmp/ImageMagick/${FILE##*/}" | less
	main
}
mono() {
	imagecheck=1
	filecheck "monochromatic"
	while [ $imagecheck = "1" ]
	do
	    cp "$FILE" "/tmp/ImageMagick/"
	    mogrify -monochrome "/tmp/ImageMagick/${FILE##*/}"
	    fileedit
	    main
	done
}
post() {
	imagecheck=1
	filecheck "posterize"
	cp "$FILE" "/tmp/ImageMagick/"
	while [ $imagecheck = "1" ]
	do
	    printf ${YELLOW}
	    echo
        echo " Change the amount of colors the image has."
	    printf ${CLEAN}
	    echo
	    cp "$FILE" "/tmp/ImageMagick/"
	    echo -n " Number of colors: "
	    printf ${GREEN}
	    if read amount
		then
		    printf ${CLEAN}
			mogrify "/tmp/ImageMagick/${FILE##*/}" -colors  $amount "/tmp/ImageMagick/${FILE##*/}"
		else
		    printf ${CLEAN}
			main
		fi
		printf ${CLEAN}
		fileedit
	done
}
res() {
	imagecheck="1"
	filecheck "resize"
	cp "$FILE" "/tmp/ImageMagick/"
	while [ $imagecheck = "1" ]
	do
	    echo
	    echo -n " Width: "
	    printf ${GREEN}
	    read width
	    printf ${CLEAN}
	    echo -n " Height: "
	    printf ${GREEN}
	    read height
	    printf ${CLEAN}
	    echo
	    echo -n " Keep aspet ratio?: "
	    read ratio
	    case $ratio in
	        1|y*|Y*) mogrify -resize "$width"x"$height" "/tmp/ImageMagick/${FILE##*/}" ;;
	        2|n*|N*) mogrify -resize "$width"x"$height"! "/tmp/ImageMagick/${FILE##*/}" ;;
	        *|b*|B*|m*|M*|q*|Q*) main ;;
	    esac	    
	    fileedit
	done
}
#####[Main]#####
main
