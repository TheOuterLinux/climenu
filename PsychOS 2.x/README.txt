# climenu
A CLI Menu for Linux command line

Packages required to run all items in climenu. At the very least, you will need dialog to open the menu to take a look.
- dialog
- mc (midnight commander)
- bashmount (https://github.com/jamielinux/bashmount/archive/master.zip)
- alsamixer
- vlock
- systemctl
- concalc
- gnuplot
- gpg2
- cdw
- htop
- nano**(I'm not a vi, vim, or pico fan; pico leaves save files behind.)
- spell
- sc (Spreadsheet Calculator)
- split
- termdown (sudo pip install termdown)
- units
- alien
- hexedit
- octave
- python-idle
- python3-idle
- bsd-games
- scholar.py (https://github.com/ckreibich/scholar.py/archive/master.zip)
- gperiodic
- w3m
- jed
- gnuchess
- gnugo
- Blender (https://www.blender.org/; you can render images from command line)
- ImageMagick
- fbi
- fbiterm
- evince**(Evince comes with most Linux distributions)
- viewnior**(Do not use feh as an alternative; images converted to .bmp via ImageMagic/mogrify will not open with feh.)
- aria2
- alpine (this replaced mailx for now; still looking)
- termfeed (sudo pip install termfeed)
- irssi
- GeoIP
- iptraf-ng
- iftop (may need to kill with htop after quiting)
- Movgrab (https://github.com/ColumPaget/Movgrab/archive/master.zip)
- mps-youtube (sudo pip install mps-youtube)
- youtube-dl (sudo pip install youtube-dl and grab package manager version too if you can)
- NetworkManager
- rainbowstream (sudo pip install rainbowstream; climenu will check for you)
- rtv (sudo pip install rtv; climenu will check for you)
- zenity
- xsel
- asciinema (sudo pip3 install asciinema)
- ffmpeg
- moc (the command line music player, not the Qt development whatever; It's much better than cmus; start anytime with "mocp".)
- vlc (Only to have a DVD menu in GUI mode)
- mpv (Used in TTY to play video via --vo=opengl or caca)
	*DirectFB
	*DirectFB-Mesa
	*DirectFB-libSDL
- aspell
- classifier (sudo pip install classifier)
- An openoffice (LibreOffice) program to run 'soffice'
- poppler-tools
- lyx (Only to convert the Lyx template files to PDF)
- SC-IM (https://github.com/andmarti1424/sc-im/archive/freeze.zip)
- solaar-cli
- powertop
- yast (OpenSUSE settings manager GUI and command line)
- audit
- bleachbit
- clamav
- Lynis (https://github.com/CISOfy/lynis/archive/master.zip)
- sensors
- zypper (OpenSUSE package manager; you'll have to change to match your system)

**Packages marked with this aren't neccessarily required as long as you edit the correct script files to use your preferred software.

To start climenu, technically all you need to do is cd to the folder and run ./programs-list file. You may need to chmod +x them all. 

Just please remember that this was originally designed for PsychOS here: https://theouterlinux.com/psychos. A "journey journal" of sorts can be found here: https://github.com/TheOuterLinux/PsychOS. PsychOS is OpenSUSE 13.2, 32-bit based and not Ubuntu-based like everything else; it's faster, runs the latest kernel (install "kernel-pae" for 4+GB RAM usage) after the Upgrade script, and doesn't lag on older computers, even if you only have a 1 GB of RAM laptop from 8 years ago (remember netbooks?). PsychOS was built for personal research, but includes other things to make it more "out-of-the-box" and not so boring.

Climenu is included with the Upgrade script for PsychOS (find it here: https://theouterlinux.com/psychos). Though, it technically installs the folder to /opt/ and then a script called "climenu" to /usr/bin/. So for best compatibility, install PsychOS and then run the Upgrade script. Afterwards, you'll just type "climenu" in the command line. It works for both GUI and TTY environments. Don't forget to replace YaST and zypper with your own settings and package manager (example: apt-get); these were made for OpenSUSE.
