#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
_alarm() {
		( \speaker-test --frequency $1 --test sine )&
		pid=$!
		\sleep ${2}s
		\kill -9 $pid
			}
HEIGHT=20
WIDTH=60
CHOICE_HEIGHT=20
BACKTITLE="Programs List --> Accessories"
TITLE="Accessories"
MENU="Choose one of the following options:"

OPTIONS=(1 "<-- Go back"
         2 "concalc - command line calculator"
         3 "gnuplot - plot a graph in the terminal"
         4 "GPG - encrypt/decrypt files with GPG"
         5 "cdw - Burn files to disc, create image, or wipe."
         6 "htop - view and edit running processes"
         7 "nano - a simple text editor"
         8 "sc - spreadsheet calculator"
         9 "split - file splitting"
         10 "termdown - timer & stopwatch"
         11 "units - convert measurements and currencies (2014)")

CHOICE_ACCESSORIESMENU=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

clear
case $CHOICE_ACCESSORIESMENU in
        1)
            cd $DIR
            ./programs-list
            ;;
        2)
            while :
			do
				if input=$(dialog --inputbox "Enter calculation. Use SHIFT+CTRL+V to paste." 10 75 3>&1 1>&2 2>&3)
				then
					output=$(concalc "$input")
					dialog --msgbox "Answer: $output" 10 75
				else
					cd $DIR
					./accessories-list
				fi
			done
            cd $DIR
			./accessories-list
			exec 3>&-
            ;;
        3)
			gnuplot
            cd $DIR
			./accessories-list
			exec 3>&-
            ;;
        4)
			cd $DIR
			dialog --msgbox "WARNING:\nEncrypted files via gpg may be keychained on your system. This means other people using the same computer may be able to open gpg file without a password." 10 75
			if encryptdecrypt=$(dialog --backtitle "$BACKTITLE --> Encrypt/Decrypt File (GPG)" --title "Please choose an option" --menu "" 8 25 20 \
				1 "Encrypt file" \
				2 "Decrypt file" 3>&1 1>&2 2>&3)
			then
				case $encryptdecrypt in
					1)
						foldercheck="directory"
						while [ $foldercheck = "directory" ]
						do
							if location=$(dialog --stdout --backtitle "$BACKTITLE --> Decrypt File (GPG)" --title "Please choose a file to ENCRYPT:" --fselect $HOME/ 10 75)
							then
								foldercheck=$(file -b "$location" | grep "")
								clear
								gpg -c "$location"
								dialog --msgbox "File $location was encrypted."
								clear
							else
								cd $DIR
								./accessories-list
							fi
							if [ $foldercheck = "directory" ]
							then
								dialog --msgbox "Please choose a file and not a folder." 10 60
							fi
						done
						;;
					2)
						foldercheck="directory"
						while [ $foldercheck = "directory" ]
						do
							if location=$(dialog --stdout --backtitle "$BACKTITLE --> Decrypt File (GPG)" --title "Please choose a file to DECRYPT:" --fselect $HOME/ 10 75)
							then
								foldercheck=$(file -b "$location" | grep "")
								clear
								gpg -d "$location"
								rm -rf "$location"
								dialog --msgbox "File $location was decrypted and encrypted $location was deleted." 10 75
								clear
							else
								cd $DIR
								./accessories-list
							fi
							if [ $foldercheck = "directory" ]
							then
								dialog --msgbox "Please choose a file and not a folder." 10 60
							fi
						done
						;;
				esac
			else
				cd $DIR
				./accessories-list
			fi
			cd $DIR
			./accessories-list
			exec 3>&-
            ;;
        5)
            cdw
            cd $DIR
			./accessories-list
			exec 3>&-
            ;;
        6)
            htop
            cd $DIR
			./accessories-list
			exec 3>&-
            ;;
        7)
			cd ~/Documents
            nano
            cd $DIR
			./accessories-list
			exec 3>&-
            ;;
        8)
			cd ~/Documents
            sc
            cd $DIR
			./accessories-list
			exec 3>&-
            ;;
        9)
            foldercheck="directory"
			while [ $foldercheck = "directory" ]
			do
				if location=$(dialog --stdout --backtitle "$BACKTITLE --> Split file" --title "Please choose a file to split:" --fselect $HOME/ 10 75)
				then
					foldercheck=$(file -b "$location" | grep "")
					folder=$(dirname "$location")
					filename="${location##*/}"
					rawsize=$(wc -c "$location" | awk '{print $1}')
					correctedsize=$(echo "scale=6; $rawsize / 1000000" | bc)
				else
					cd $DIR
					./accessories-list
				fi
				if [ $foldercheck = "directory" ]
				then
					dialog --msgbox "Please choose a file and not a folder." 10 60
				fi
			done
			if option=$(dialog --backtitle "$BACKTITLE --> Convert" --title "Please choose an option" --menu "" 12 40 20 \
				1 "Split by size" \
				2 "Split file every 'n' lines" \
				3 "Split by 'n' number of files" 3>&1 1>&2 2>&3)
			then
				case $option in
					1)
						error="1"
						while [ $error = "1" ]
						do
							if number=$(dialog --inputbox "Enter file size (MB) to split:\n \n'$filename' is $correctedsize MB in size.\n \n1 MB is 1000 KB in this case." 10 50 3>&1 1>&2 2>&3)
							then
								if echo "$number" | egrep -q '^[0-9]*\.?[0-9]+$'
								then
									error="0"
									convertednumber=$(echo "scale=0; $number * 1000000 / 1" | bc)
									split -d -b "$convertednumber" "$location" "$folder/$filename"
									dialog --msgbox "'$filename' was split." 6 50
								else
									dialog --msgbox "$number is an invalid entry. Only positive whole numbers accepted." 10 75
								fi
							else
								cd $DIR
								./accessories-list
							fi
						done
						;;
					2)
						error="1"
						while [ $error = "1" ]
						do
							if number=$(dialog --inputbox "Split file every 'n' lines:" 10 50 3>&1 1>&2 2>&3)
							then
								if [[ "$number" = +([0-9]) ]]
								then
									error="0"
									split -d -l "$number" "$location" "$folder/$filename"
									dialog --msgbox "'$filename' was split." 6 50
								else
									dialog --msgbox "$number is an invalid entry. Only positive whole numbers accepted." 10 75
								fi
							else
								cd $DIR
								./accessories-list
							fi
						done
						;;
					3)
						error="1"
						while [ $error = "1" ]
						do
							if number=$(dialog --inputbox "Split by 'n' number of files:\nThere may be an extra file in the split." 10 50 3>&1 1>&2 2>&3)
							then
								if [[ "$number" = +([0-9]) ]]
								then
									error="0"
									splitsize=$(echo "scale=0; $rawsize / $number" | bc)
									split -d -b "$splitsize" "$location" "$folder/$filename"
									dialog --msgbox "'$filename' was split." 6 50
								else
									dialog --msgbox "$number is an invalid entry. Only positive whole numbers accepted." 10 75
								fi
							else
								cd $DIR
								./accessories-list
							fi
						done
						;;
				esac
			else
				cd $DIR
				./accessories-list
			fi
            cd $DIR
			./accessories-list
			exec 3>&-
            ;;
        10)
			if option=$(dialog --backtitle "$BACKTITLE --> Termdown" --title "Please choose an option" --menu "" 12 40 20 \
				1 "Timer with alarm" \
				2 "Timer no alarm" \
				3 "Stopwatch" 3>&1 1>&2 2>&3)
			then
				case $option in
					1)
						if time=$(dialog --inputbox "Enter Timer (seconds)" 10 75 3>&1 1>&2 2>&3)
						then
							termdown -o /tmp/tempdownlog.txt "$time"
							if [ -t 0 ]; then stty -echo -icanon -icrnl time 0 min 0; fi
							count=0
							keypress=''
							while [ "x$keypress" = "x" ]; do
								let count+=1
								for count in $(seq 0 1 1) ; do sleep 1; echo $count | dialog --gauge "Press any key to stop alarm" 6 35 1; done
								echo "Press any key to stop alarm."
								echo ""
								echo -ne $count'\r'
								_alarm 400 .2
								_alarm 800 .2
								_alarm 1000 .2
								keypress="`cat -v`"
								clear
							done
							if [ -t 0 ]; then stty sane; fi
							clear
							cd $DIR
							./accessories-list
						else
							cd $DIR
							./accessories-list
						fi
						;;
					2)
						if time=$(dialog --inputbox "Enter Timer (seconds):" 10 75 3>&1 1>&2 2>&3)
						then
							termdown -o /tmp/tempdownlog.txt "$time"
							dialog --msgbox "Time is up." 6 30
							cd $DIR
							./accessories-list
						else
							cd $DIR
							./accessories-list
						fi
						;;
					3)
						dialog --msgbox "Timer set for: $time\nL - lap\nSPACE - pause\nR - reset\nQ - quit" 10 40
						termdown -o /tmp/tempdownlog.txt #No time set turns termdown into a stopwatch
						echo ""
						echo "Finished. Press ENTER to continue."
						read ""
						;;
				esac
			else
				cd $DIR
				./accessories-list
			fi
            cd $DIR
			./accessories-list
			exec 3>&-
            ;;
        11)
			dialog --title "Units history. (/tmp/unitslog.txt) Exit to continue" --textbox /tmp/unitslog.txt 32 80
			clear
			echo "Use CTRL+D to exit program."
			units --verbose --log /tmp/unitslog.txt
			cd $DIR
			./accessories-list
			exec 3>&-
            ;;
        255)
            cd $DIR
			./programs-list
            ;;
esac
