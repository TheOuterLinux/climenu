###########
# Algebra #
###########

+ Basic Properties and Facts
++++++++++++++++++++++++++++

	| Distance Formula
	------------------
			  _________________
		d(p1,p2)=√(x2-x1)²+(y2-y1)²
		
	| Complex Numbers
	-----------------
		   __
		i=√-1
		i²=-1
		       ____
		(-a)²=√i(a)
		
+ Logarithms and Log Properties
+++++++++++++++++++++++++++++++

     __________________________________________________________________
  4 |        |   |                                   |  ___,..-->      |
    |        |   |                      log (x),,..--|''               |
    |        |   |                    _,.,-2''       |                 |
  3 |        |   |             _,.--''               |            ___> |
    |        |   |        _,--'       log (x)    _.__|...----'''''     |
    |        |   |    ,,-'           _,,.e.---`''    |                 |
  2 |        |   |_.-'      __..---''                |                 |
    |        |  ,|    _,,-''                         |                 |
    |-------------------------------------------------------------------
  1 |      / |,-'|             _____......--------'"'|'''''>           |
    |     /.-|   |,,..,---'''''      log  (x)        |                 |
    |    /.,.+-''|                      10           |                 |
  0 |==[1]==[2]=[e]=============[5]================[10]===========[X]==|
    | ',,                                                              |
 -1 |/,                                                                |
    |                                                                  |
    |                                                                  |
 -2 |                                                                  |
    |                                                                  |
    |                                                                  |
 -3 |                                                                  |
    |                                                                  |
    |                                                                  |
    |                                                                  |
  -4|                                                                  |
    `------------------------------------------------------------------


	| Definition
	------------
		y = log sub b of x is equivalent to 
			x = b^y
		--------------------------------------
		[Example: log sub 5 of 125 = 3 because
		5³=125]
		--------------------------------------
		
	| Special Logarithms
	--------------------
		ln x = logₑx = natural log
		log x = log sub 10 of x = common log
		ₑ = 2.718281828
		
	| Logarithm Properties
	----------------------
		log sub b of b = 1
		lob sub b of 1 = 0
		log sub b of b^x = x
		b^(log sub b of x) = x
		log sub b of (x^r) = r*log sub b of x
		log sub b of (xy) = (log sub b of x) + (log sub b of y)
		log sub b of (x/y) = (log sub b of x) - (log sub b of y)
		
		* The domain of log sub b of x is > 0
		
+ Factoring and Solving
+++++++++++++++++++++++
	
	| Factoring Formulas
	--------------------
		(x+a)(x-a) = (x²)-(a²)
		(x+a)² = (x²)+(2ax)+(a²)
		(x-a)² = (x²)-(2ax)+(a²)
		(x+a)(x+b) = (x²)+(a+b)x+(ab)
		(x+a)³ = (x³)+(3ax²)+(3a²x)+(a³)
		(x-a)³ = (x³)-(3ax²)+(3a²x)-(a³)
		
	| Quadratic Formula
	-------------------
		(ax²)+(bx)+c = 0, a ≠ 0
		    ____________
		-b±√((b²)-(4ac))
		    ¯¯¯¯¯¯2¯¯¯¯¯


+ Functions and Graphs
++++++++++++++++++++++

	| Constant Function
	-------------------
		y=a or f(x)=a
		
	| Line/Linear Function
	----------------------
		y=mx+b or f(x)=mx+b
		
                                  |                         ,-'
                                  |                     _.-'
                                  |                  ,,'
                               +Y |               ,-'
                                  |           _,-'
                                  |        _,'
                                  |     ,-'
                                  | _,-'
                               b _,'
                              ,,' |
                           ,-'    |
                       _.-'       |
....................,,:...........|.....................................
   -X            ,-'              |                                 +X
             _.-'                 |
          _,'                     |
       ,-'                        |
   _,-'                           |
_,'                            -Y |
                                  |
                                  |

		slope
		-----
		[intercept form]: rise/run, 
			or m=   (y2-y1)
			      ¯¯(x2-x1)¯¯
		
		[point-slope form]: y=y1+m(x−x1)
		
	| Parabola/Quadratic
	--------------------
		y=ax²+bx+c or f(x)ax²+bx+c
		
		x=ay²+by+c or g(y)=ay²+by+c


             |                |+Y              |
             |                |                |
              |               |               |
              |               |               |
              |               |               |
               \              |              /
                \             |             /
                '.            |            ,'
                 `.           |           ,'
                   \          |          /
                    `.        |        ,'
                      `._     |     _,'
.........................:-....,,.-:............................
    -X                        |C                      +X
                              |
                              |
                              |
                              |
                              |
                              |
                              |-Y
                              |

	| Circle
	--------
		(x−h)+(y−k)=r²
		--------------------------------
		[Graph is a circle with radius r 
		and center(h,k)]
		--------------------------------
                                  |
                                  |+Y
                                  |
                           __..---+---..__
                       _.-'       |       `-._
                     ,'           |           `.
                   ,'             |             `.
                 ,'               |               `.
                /                 |                 \
               /                  |                  \
              /                   |                   \
   -X         /                   |                   \   _       +X
..............|...................|(h,k)..............|.[√r²]...........
              |                   |                   |
              |                   |                   |
              `.                  |                  .'
               |                  |                  '
                `.                |                ,'
                  \               |               /
                   `.             |             ,'
                     `-.          |          ,-'
                        `-.._     |     _,.-'
                             `'---+---''
                                  |
                                  |
                                  |-Y
                                  |


	| Elipse
	--------
		(((x-h)²)/(a²)) + (((y-k)²)/(b²)) = 1
		-----------------------------------------
		[Graph is an ellipse with center ( h , k ) 
		with vertices a units right/left from the 
		center and vertices b units up/down from 
		the center.]
		------------------------------------------
		
	| Hyperbola
	-----------
		(((y-k)²)/(b²)) - (((x-h)²)/(a²)) = 1
		---------------------------------------------
		[Graph is a hyperbola that opens up and down, 
		has a center at (h,k), vertices b units 
		up/down from the center and asymptotes that 
		pass through center with slope ± b/a.]
		---------------------------------------------

,                                 |                                  ,'
 `:                               | +Y                             .'
   `.                             |                              ;'
     `.                           |                            ;'
       `.                         |                          ,'
         `.                       |                         /
           \                      |                       ,'
            \                     |                      /
             \                    |                    .'
              \                   |                   .'
               \                  |                   |
                |                 |                  |
                |                 |                  |
                 |                |                 |
                 |                |                 |
-----------------+----------------+ ----------------+------------------
   -X            |                |                 |             +X
                .'                |                 '.
                |                 |                  |
               .'                 |                  '.
               '                  |                   '.
              /                   |                     \
            ,'                    |                      \
           /                      |                       \
         ,'                       |                        `.
       ,'                         |                          `.
     ,'                           |                            `.
   ,'                             |                              `.
 ,'                               | -Y                             `.
'                                 |                                  `
