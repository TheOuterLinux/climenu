#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
HEIGHT=20
WIDTH=60
CHOICE_HEIGHT=20
BACKTITLE="Programs List --> System"
TITLE="System"
MENU="Choose one of the following options:"

OPTIONS=(1 "<-- Go back"
		 2 "aureport - view system audit reports"
		 3 "Bleachbit - clean system caches"
         4 "Clamscan - scan for threats"
         5 "Freshclam - update malware definitions"
         6 "htop - view and edit running processes"
         7 "Lynis - scan for rootkits and security suggestions"
         8 "nmcli - Network/Internet Manager"
         9 "powertop - monitor and manage power consumption"
         10 "sensors - monitor temperatures"
         11 "Zypper - update software")

CHOICE_SYSTEMMENU=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

clear
case $CHOICE_SYSTEMMENU in
        1)
            cd $DIR
            ./programs-list
            ;;
        2)
			echo "Root permissions needed for aureport."
			sudo aureport
			echo ""
			echo ""
			echo "Finished. Press ENTER to go back."
			read ""
            cd $DIR
            ./system-list
            ;;
        3)
            cd $DIR
            ./bleachclean
            ;;
        4)
			mkdir /tmp/ClamAV
			mkdir /tmp/ClamAV/Quarantine
			firsterror="1"
			while [ $firsterror = "1" ]
			do
				if folder=$(dialog --stdout --backtitle "$BACKTITLE --> Simple ClamAV Scanner" --title "Please choose a folder to scan" --dselect / 10 75)
				then
					echo ""
				else
					cd $DIR
					./system-list
				fi
				echo ""
				echo -n "Was $folder the correct folder to start scanning? Scans by default are recursive and verbose. (y/n/q to quit): "
				read foldercheck
				if [ $foldercheck = "y" ] || [ $foldercheck = "yes" ] || [ $foldercheck = "Y" ] || [ $foldercheck = "Yes" ] || [ $foldercheck = "YES" ]
				then
					firsterror="0"
				elif [ $foldercheck = "n" ] || [ $foldercheck = "no" ] || [ $foldercheck = "N" ] || [ $foldercheck = "No" ] || [ $foldercheck = "NO" ]
				then
					clear
				elif [ $foldercheck = "q" ] || [ $foldercheck = "Q" ]
				then
					cd $DIR
					./system-list
				else
					clear
					echo "$foldercheck is not a valid option. Please try again."
					echo ""
				fi
			done
			clear
			seconderror="1"
			while [ $seconderror = "1" ]
			do
				echo -n "Would you like to Quarantine infected files if found? (y/n/q to quit)."
				echo "Infected files will be moved to /tmp/ClamAV/Quarantine"
				read answer
				if [ $answer = "y" ] || [ $answer = "yes" ] || [ $answer = "Y" ] || [ $answer = "Yes" ] || [ $answer = "YES" ]
				then
					clear
					echo "TIP: Though not recommended, you can continue to do other work while scanning by using Alt+F1,F2,F3,...to open more terminals and same keys to switch between them."
					echo "About to start scanning. Need root permissions."
					sudo clamscan -v -r --tempdir="/tmp/ClamAV" --log="/tmp/ClamAV/clamscanlog.txt" "$folder"
					seconderror="0"
				elif [ $answer = "n" ] || [ $answer = "no" ] || [ $answer = "N" ] || [ $answer = "No" ] || [ $answer = "NO" ]
				then
					clear
					echo "TIP: Though not recommended, you can continue to do other work while scanning by using Alt+F1,F2,F3,...to open more terminals and same keys to switch between them."
					echo "About to start scanning. Need root permissions."
					sudo clamscan -v -r --tempdir="/tmp/ClamAV" --log="/tmp/ClamAV/clamscanlog.txt" "$folder"
					seconderror="0"
				elif [ $answer = "q" ] || [ $answer = "Q" ]
				then
					cd $DIR
					./system-list
				else
					clear
					echo "$answer is not a valid option. Please try again."
					echo ""
				fi
			done
			echo ""
			echo ""
			echo "Finished scanning folder $folder. Please take a moment to review above findings."
			echo "A log was kept in /tmp/ClamAV/clamscanlog.txt"
			echo "Press ENTER to go back to System menu."
            cd $DIR
			./system-list
			exec 3>&-
            ;;
        5)
			sudo clamd stop
			sudo freshclam
			sudo clamd start
			echo ""
			echo ""
			echo "Finished with freshclam. Press ENTER to go back to System menu."
            read ""
            cd $DIR
			./system-list
			exec 3>&-
            ;;
        6)
            htop
            cd $DIR
			./system-list
			exec 3>&-
            ;;
        7)
			sudo lynis audit system | more
			echo ""
			echo ""
			echo "Finished with lynis. Press ENTER to go back to System menu."
            read ""
			cd $DIR
			./system-list
			exec 3>&-
            ;;
        8)
			if choice=$(dialog --backtitle "$BACKTITLE --> Network/Internet Manager" --title "Please choose an option" --menu "" 12 40 20 \
				1 "List devices" \
				2 "List connection profiles" \
				3 "Scan for wireless routers" \
				4 "Setup new wireless connection" \
				5 "Turn all wifi on/off" \
				6 "Connection inormation" 3>&1 1>&2 2>&3)
			then
				case $choice in
					1)
						clear
						nmcli -p d
						echo ""
						echo "Finished listing devices. Press ENTER to continue."
						read ""
						;;
					2)
						clear
						nmcli -p c
						echo ""
						echo "Finished listing connection profiles. Press ENTER to continue."
						read ""
						;;
					3)
						clear
						echo "Root privileges needed to scan for wireless routers."
						if device=$(dialog --inputbox "Enter device to use for scanning. Example: wlan0" 10 75 3>&1 1>&2 2>&3)
						then
							echo ""
						else
							cd $DIR
							./system-list
						fi
						clear
						sudo iw d "$device" scan | grep "SSID"
						echo ""
						echo "Finished scan of wireless routers. Press ENTER to continue."
						read ""
						;;
					4)
						clear
						if connectionname=$(dialog --inputbox "Enter a name for the new connection." 10 75 3>&1 1>&2 2>&3)
						then
							echo ""
						else
							cd $DIR
							./system-list
						fi
						if SSID=$(dialog --inputbox "Enter SSID of wireless router." 10 75 3>&1 1>&2 2>&3)
						then
							echo ""
						else
							cd $DIR
							./system-list
						fi
						if pass=$(dialog --insecure --passwordbox "Enter password of wireless router." 10 75 3>&1 1>&2 2>&3)
						then
							echo ""
						else
							cd $DIR
							./system-list
						fi
						nmcli d wifi c "$connectionname" password "$pass" name "$SSID"
						echo ""
						echo "Finished setting up new wireless connection. Press ENTER to continue."
						read ""
						;;
					5)
						clear
						if switch=$(dialog --backtitle "$BACKTITLE --> Network/Internet Manager" --title "Wifi On/Off switch" --menu "" 12 40 20 \
							1 "On" \
							2 "Off" 3>&1 1>&2 2>&3)
						then
							case $switch in
								1)
									nmcli r all on
									;;
								2)
									nmcli r all off
									;;
							esac
						else
							cd $DIR
							./system-list
						fi
						;;
					6)
						clear
						nmcli dev show
						echo ""
						echo "Finished showing connection information. Press ENTER to continue."
						read ""
						;;
				esac
			else
            cd $DIR
            ./system-list
            fi
            cd $DIR
            ./system-list
            exec 3>&-
            ;;
        9)
			echo "Powertop requires root privileges."
            sudo powertop
            cd $DIR
			./system-list
			exec 3>&-
            ;;
        10)
			output=$(sensors > /tmp/sensorlog.txt)
			dialog --title "Sensors" --textbox /tmp/sensorlog.txt 32 80
			rm -rf /tmp/sensorlog.txt
            cd $DIR
			./system-list
			exec 3>&-
            ;;
        11)
			sudo zypper update
            echo ""
			echo ""
			echo "Finished with zypper update."
			echo "If the kernel was updated, it is recommended to reboot the system as soon as possible."
			echo "Press ENTER to go back to System menu."
            read ""
			cd $DIR
			./system-list
			exec 3>&-
            ;;
        255)
            cd $DIR
            ./programs-list
            ;;
esac
